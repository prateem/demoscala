{
  description = "Demo Scala-3 project.";
  inputs = {
    sbt.url = "github:zaninime/sbt-derivation/92d6d6d825e3f6ae5642d1cce8ff571c3368aaf7";
  };
  outputs = {self, nixpkgs, sbt}: {
    packages = builtins.mapAttrs(system: pkgs: {
      demoscala = sbt.mkSbtDerivation.${system} {
        pname = "demoscala";
        version = "0.0.1";
        src = ./.;
        depsSha256 = "GwSQ+aH3/vieDIKz4JfDN9QTda4N+IS0aQTMP28z4Xk=";
        buildPhase = ''
          sbt compile
        '';
      };
    }) nixpkgs.legacyPackages;
    defaultPackage = builtins.mapAttrs(_: packages: packages.demoscala) self.packages;
  };
}
